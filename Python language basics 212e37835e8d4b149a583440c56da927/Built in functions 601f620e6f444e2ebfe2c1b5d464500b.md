<!-- # Built in functions -->

Basics

![Screenshot 2022-07-15 at 5.52.40 PM.png](Built%20in%20functions%20601f620e6f444e2ebfe2c1b5d464500b/Screenshot_2022-07-15_at_5.52.40_PM.png)



![Screenshot 2022-07-15 at 5.53.31 PM.png](Built%20in%20functions%20601f620e6f444e2ebfe2c1b5d464500b/Screenshot_2022-07-15_at_5.53.31_PM.png)

---

### We can also execute functions on literal data in python

## String data

![Screenshot 2022-07-15 at 5.56.11 PM.png](Built%20in%20functions%20601f620e6f444e2ebfe2c1b5d464500b/Screenshot_2022-07-15_at_5.56.11_PM.png)

## Array data

![Screenshot 2022-07-15 at 5.57.54 PM.png](Built%20in%20functions%20601f620e6f444e2ebfe2c1b5d464500b/Screenshot_2022-07-15_at_5.57.54_PM.png)
