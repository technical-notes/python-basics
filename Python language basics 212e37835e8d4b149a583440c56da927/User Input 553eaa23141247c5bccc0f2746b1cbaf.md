<!-- # User Input -->

![Screenshot 2022-07-09 at 8.23.14 PM.png](User%20Input%20553eaa23141247c5bccc0f2746b1cbaf/Screenshot_2022-07-09_at_8.23.14_PM.png)

- For accepting user inputs, python provides a built in function `input()`
- `input()` accepts argument for the text to be shown
- this function can then be assigned to a variable to capture the input

```python
user_name = input("Hi! What's your name?\n")
print(f'hello {user_name}. Pleased to meet you.')

num = input("Enter a number to be squared. \n")
square = int(num) * int(num)
print(f'The square of {num} is {square}')
```

Output:

![Screenshot 2022-07-09 at 8.56.24 PM.png](User%20Input%20553eaa23141247c5bccc0f2746b1cbaf/Screenshot_2022-07-09_at_8.56.24_PM.png)

---

# Function with return values

Function can return any values by `return` keyword

 
The return statement
`return_stmt ::=  "return" [expression_list]`

return may only occur syntactically nested in a function definition, not within a nested class definition.
If an expression list is present, it is evaluated, else None is substituted.
return leaves the current function call with the expression list (or None) as return value.
When return passes control out of a try statement with a finally clause, that finally clause is executed before really leaving the function.
In a generator function, the return statement indicates that the generator is done and will cause `StopIteration` to be raised.
The returned value (if any) is used as an argument to construct `StopIteration` and becomes the `StopIteration.value` attribute.


Example flow:

![Screenshot 2022-07-09 at 9.07.24 PM.png](User%20Input%20553eaa23141247c5bccc0f2746b1cbaf/Screenshot_2022-07-09_at_9.07.24_PM.png)
