<!-- # Functions & Scopes -->

- A function is defined using the `def` keyword.

```python
units_formulae = 24 * 60
unit_name = "minutes"

def days_to_units(no_of_days, message):
    print(f"{no_of_days} days are {no_of_days * units_formulae} {unit_name}")
    print(message)

days_to_units(2, "great")
days_to_units(5, "awesome")
days_to_units(7, "jhakaas")
```

Output

```python
2 days are 2880 minutes
great
5 days are 7200 minutes
awesome
7 days are 10080 minutes
jhakaas
```

# Scopes

![Screenshot 2022-07-09 at 7.59.58 PM.png](Functions%20&%20Scopes%20fdd2cb0fe47e41e4b9764e92b974adde/Screenshot_2022-07-09_at_7.59.58_PM.png)

- Variables from a different scope can not be accessed.

![Screenshot 2022-07-09 at 8.01.57 PM.png](Functions%20&%20Scopes%20fdd2cb0fe47e41e4b9764e92b974adde/Screenshot_2022-07-09_at_8.01.57_PM.png)
