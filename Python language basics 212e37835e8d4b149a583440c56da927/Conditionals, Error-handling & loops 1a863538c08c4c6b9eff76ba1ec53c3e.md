<!-- # Conditionals, Error-handling & loops -->

# Conditionals

![Screenshot 2022-07-10 at 2.16.52 AM.png](Conditionals,%20Error-handling%20&%20loops%201a863538c08c4c6b9eff76ba1ec53c3e/Screenshot_2022-07-10_at_2.16.52_AM.png)

```python
def my_validator_func(input_num):
    if input_num > 0:
        return f"{input_num} is greater than 0"
    else:
        return f"{input_num} is negative"

res = my_validator_func(-4)
print(res)
```

- Conditions in `if` statement is evaluated as Boolean data type value `True` or `False` .



<aside>
💡 In Python, there is no small case true/false for Boolean values. Its `True` & `False`

</aside>

## Multiple if-else conditions

To check multiple if-else condition we can use `elif` keyword.

```python
def my_validator_func(input_num):
    if input_num > 0:
        return f"{input_num} is a positive number"
    elif input_num == 0:
        return f"{input_num} is well a ZERO"
    else:
        return f"{input_num} is a negative number"

user_input = input('Enter a number:\n')
res = my_validator_func(int(user_input))
print(res)
```

# Checking types of variables

- `type()` function is used for this.

```python
int_val = 3
print(type(int_val))

floating_val = 3.53
print(type(floating_val))

string_val = 'Some string'
print(type(string_val))

boolean_val = True
print(type(boolean_val))
```

Output:

```
<class 'int'>
<class 'float'>
<class 'str'>
<class 'bool'>
```

---

# Error handling with `try/except`

![Screenshot 2022-07-10 at 3.19.38 AM.png](Conditionals,%20Error-handling%20&%20loops%201a863538c08c4c6b9eff76ba1ec53c3e/Screenshot_2022-07-10_at_3.19.38_AM.png)

- Similar to try and catch in other languages.

```python
def division_func(var1, var2):
    try:
        int_var1 = int(var1)
        int_var2 = int(var2)

        print('Performing Division operation')
        res = int_var1 / int_var2

        print(f'result for operation: {res}')
    except ValueError:
        print('Error: Invalid input provided')
    except ZeroDivisionError:
        print('Error: Dont divide by zero')
    except:  # catch all error
        print('Generic error handling')
    finally:
        print('This will always execute')

print('Welcome to Division program unit')
num1 = input('Enter first number (Numerator)\n')
num2 = input('Enter second number (Denominator)\n')

division_func(num1, num2)
```

---

# Loops

While loop

```tsx
def loop_func(seed):
    num = seed
    while num < 10:
        num = num + 1
        print(num)

loop_func(3)
```

## List data type

- Array data type in python. Represented by `[]`

```python
my_list = [1, 2, 3, 5, 8]

for val in my_list:
    double = int(val) * 2
    print(double)

```

- We can also accept a string and split it which will create the list

![Screenshot 2022-07-11 at 8.44.55 PM.png](Conditionals,%20Error-handling%20&%20loops%201a863538c08c4c6b9eff76ba1ec53c3e/Screenshot_2022-07-11_at_8.44.55_PM.png)

```python
my_list = '100, 200, 300'

for val in my_list.split(','):
    double = int(val) * 2
    print(double)
print('loop finished')
```

## List operations

![Screenshot 2022-07-11 at 8.51.53 PM.png](Conditionals,%20Error-handling%20&%20loops%201a863538c08c4c6b9eff76ba1ec53c3e/Screenshot_2022-07-11_at_8.51.53_PM.png)

```python
fruits = ['Apple', 'Banana', 'Cherry']

print(fruits[1])  # Banana
print(fruits)

# add elements
fruits.append("Mango")
print(fruits)

# remove all elements
fruits.clear()
print(fruits)

# sort elements
fruits = ['Cherry', 'Apple', 'Banana']
print(fruits)
fruits.sort()
print(fruits)
```

# Comments

- multi line comment in python is done by `"""`  (3 quotes) at the start and end.

# Set

- Set data type can be created from a list using `set()` function

```python
my_list = [1, 2, 3, 1, 3, 4, 2, 5]

my_set = set(my_list)
print(my_set)
print(type(my_set))

my_list_1 = list(my_set)
print(my_list_1)
print(type(list(my_set)))
```

Output

```python
{1, 2, 3, 4, 5}
<class 'set'>
[1, 2, 3, 4, 5]
<class 'list'>
```

## Set Properties

![Screenshot 2022-07-15 at 5.40.42 PM.png](Conditionals,%20Error-handling%20&%20loops%201a863538c08c4c6b9eff76ba1ec53c3e/Screenshot_2022-07-15_at_5.40.42_PM.png)
