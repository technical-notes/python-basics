<!-- # Modules & Packages -->

## To import complete module

```python
import <module_name >
```

## To import specific function from another python file

```python
from <class_name> import <function_name>
```

## Use module_name as different word

```python
 import module_name as m
```

# Built-in module

![Screenshot 2022-07-16 at 7.16.38 PM.png](Modules%20&%20Packages%205add5a0be61c4bd2a78193bc4fe0a47d/Screenshot_2022-07-16_at_7.16.38_PM.png)

![Screenshot 2022-07-16 at 7.17.14 PM.png](Modules%20&%20Packages%205add5a0be61c4bd2a78193bc4fe0a47d/Screenshot_2022-07-16_at_7.17.14_PM.png)

---

# Third-party modules

![Screenshot 2022-07-18 at 7.48.56 PM.png](Modules%20&%20Packages%205add5a0be61c4bd2a78193bc4fe0a47d/Screenshot_2022-07-18_at_7.48.56_PM.png)

- python modules are hosted in pypi repository. [pypi.org](http://pypi.org) (Similar to maven packages)
- module is a single file while package is a collection of modules

![Screenshot 2022-07-18 at 7.54.20 PM.png](Modules%20&%20Packages%205add5a0be61c4bd2a78193bc4fe0a47d/Screenshot_2022-07-18_at_7.54.20_PM.png)
