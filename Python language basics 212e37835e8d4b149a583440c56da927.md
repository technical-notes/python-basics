# Python language basics

[Syllabus](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Syllabus%208a066fdb26d549efa9d268f75062fe56.md)

[Basics](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Basics%209511e341e2434ef3bd8160e4840e44f6.md)

[Functions & Scopes](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Functions%20&%20Scopes%20fdd2cb0fe47e41e4b9764e92b974adde.md)

[User Input](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/User%20Input%20553eaa23141247c5bccc0f2746b1cbaf.md)

[Conditionals, Error-handling & loops](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Conditionals,%20Error-handling%20&%20loops%201a863538c08c4c6b9eff76ba1ec53c3e.md)

[Built in functions](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Built%20in%20functions%20601f620e6f444e2ebfe2c1b5d464500b.md)

[Modules & Packages](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Modules%20&%20Packages%205add5a0be61c4bd2a78193bc4fe0a47d.md)

[Classes & Objects](Python%20language%20basics%20212e37835e8d4b149a583440c56da927/Classes%20&%20Objects%20294927b8b309466ca1557eb1582490b3.md)

---